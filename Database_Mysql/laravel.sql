-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 14, 2021 at 09:10 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('0a7afd007985b184cf708c05aec70915a592b3c7c51752489eec0b601fc6906e6b342acc01ff0154', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 13:03:24', '2021-05-14 13:03:24', '2022-05-14 06:03:24'),
('0bcddcaae2e1beefe2bd6758c73afa206bebf93698c417e9b854aae6dcd1d089062248b4293ea283', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-13 10:39:17', '2021-05-13 10:39:17', '2022-05-13 03:39:17'),
('0d99fa8b25e8bdec1d2070e89e6355c5fe64aa8fb762d1ae5efab7bfce74dc51896fb2d39fa99f05', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 13:18:31', '2021-05-14 13:18:31', '2022-05-14 06:18:31'),
('0e99c23cfc1a7e482397eb8fdaadd448871b1950085294aa8928be8a0c82e439c5ed2de8adf7e30a', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-13 17:08:53', '2021-05-13 17:08:53', '2022-05-13 10:08:53'),
('0f9f417a527309b84901e67f204bf2eea2f7dab671b0bf852a49fec6d067d88bd7f04aa0f379c7e6', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 14:00:29', '2021-05-14 14:00:29', '2022-05-14 07:00:29'),
('12ea6d05fb7756d32981889e6c42031351fdda97abb945a0eef522c3b7a067cc5e9a5005731dea96', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 14:09:43', '2021-05-14 14:09:43', '2022-05-14 07:09:43'),
('17b89aa686e5c1ad26117c22d7fadcc5d61fbf9ced91cb7da0bc1bcb208d53cc2efd605bc297daad', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 13:17:29', '2021-05-14 13:17:29', '2022-05-14 06:17:29'),
('1b0f826c9071858fc6233d3f3275e567790ffb5d7aeb8eeefb6af9f5e5faab96fa73e7c557022ec1', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 14:07:11', '2021-05-14 14:07:11', '2022-05-14 07:07:11'),
('1fc45a2f01a0628f16e031bfc214aa3284a354bdf6b15666d12cf6a34c2eaeaa2a1db2bd4348b166', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 14:09:12', '2021-05-14 14:09:12', '2022-05-14 07:09:12'),
('203cefa1a5635646db016208fdba6fb3d0b55c7556af25778f4bf4e27219ae91c7718aeb9a21f783', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 13:18:27', '2021-05-14 13:18:27', '2022-05-14 06:18:27'),
('22f850497771efb6d4271dbd36929de0207f07ad44bcb091fd623c9c928e5acee877e94b83577fe0', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-13 10:22:02', '2021-05-13 10:22:02', '2022-05-13 03:22:02'),
('2b7cc9497e89bf6f34f8b8ef7ac511b5fbfe35ace19b31fc24dc5f3a8a332fcf947a75764038de54', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 13:17:16', '2021-05-14 13:17:16', '2022-05-14 06:17:16'),
('2d115810c2da9bb8e7a138d0928e2f4bf1f901ea0081686a46e30edf3f53cd7074dd7843c31722b6', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 13:59:56', '2021-05-14 13:59:56', '2022-05-14 06:59:56'),
('33f24f56fe9e280699223d98cda2075d08ac0cd8356396d59bdd6b3f209fcf845cc762d092c8fb4d', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 14:04:32', '2021-05-14 14:04:32', '2022-05-14 07:04:32'),
('4299198eaa793f9b752bd7aea1d4baf497b04570d947dcc5cf48fac00b2491ad6ab936b0cbbd9a11', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 14:04:17', '2021-05-14 14:04:17', '2022-05-14 07:04:17'),
('44b589b2ec9008d6e5dcc157c88d49ff2bf1de73123cfb143cab26abda7d065932c5b82feebeabda', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 13:18:26', '2021-05-14 13:18:26', '2022-05-14 06:18:26'),
('48840929039f1516ba4374e8d48d7aa767840ba386dcaf60714501620825acc5f76137c6ac33481c', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-13 17:07:40', '2021-05-13 17:07:40', '2022-05-13 10:07:40'),
('521d48cc2fa8170a24fd9bde0193fad5d1e86abf53ef91c9d8178364d9ebdd584e84e39a2a95977b', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 12:14:34', '2021-05-14 12:14:34', '2022-05-14 05:14:34'),
('5f5d0e04319b5804634af1a7afa71d02cb6e9cb8f9a9cb038c91ce5e25c79effda2ec3fe6fec7c01', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-13 10:36:36', '2021-05-13 10:36:36', '2022-05-13 03:36:36'),
('60399052ec94c8ab0c993ee7f8c3f54274bae3d882c1de55fe51d8b2a3d09e31f511df04c5631d99', 15, '936af4d8-990c-4137-a09d-d3ad721771af', 'MyApp', '[]', 0, '2021-05-13 16:43:09', '2021-05-13 16:43:09', '2022-05-13 09:43:09'),
('6c723e51afbd091b46d4f205350f1b48674a1fadd82c983fc5402bbd3c2a2496eed10abae8cfdae3', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-13 10:38:17', '2021-05-13 10:38:17', '2022-05-13 03:38:17'),
('6cb24e6fd58467df27d298fda4b23a246441ec3481f20045ef70fe03e25791e80cf3fb5744f85c29', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 13:18:30', '2021-05-14 13:18:30', '2022-05-14 06:18:30'),
('6e7ce839b5b424f3e34d005f168ee19aee51a379183faabaa4c15b0868bdddb49e8a6fa10affedfa', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'MyApp', '[]', 0, '2021-05-13 10:18:31', '2021-05-13 10:18:31', '2022-05-13 03:18:31'),
('7375fd76eca9861296508c2069bc2ba905bb225e46d9ab30d7e7ae5aa425dbf7cf1ce1bc697bc688', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-13 17:10:39', '2021-05-13 17:10:39', '2022-05-13 10:10:39'),
('7cbc62a775cde44952606616d3dd3a630202da0dd40c5bf3960e4351d99ec1c03d2aa59f23125a30', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 14:08:53', '2021-05-14 14:08:53', '2022-05-14 07:08:53'),
('803871936672173e38975d015a2812f5a6a7083d5d580201e9f23ecbfb12863dcaf0bc7dedde81b9', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 13:18:29', '2021-05-14 13:18:29', '2022-05-14 06:18:29'),
('8d4cac1f24062e00e4c755a467f34317638abc5f926730483250cd9bc4bb5405b8720b912c310205', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 12:15:58', '2021-05-14 12:15:58', '2022-05-14 05:15:58'),
('98b1c742d4ac27ed7fe47acb5abaee458a4f8ecc190338f6f819a94b5e90f06df9924776aebf1174', 18, '936af4d8-990c-4137-a09d-d3ad721771af', 'MyApp', '[]', 0, '2021-05-13 16:44:49', '2021-05-13 16:44:49', '2022-05-13 09:44:49'),
('9907d285e2b5d70734c42083aaf5823d0ee1059e42f5bf82a89b23ef64c05f88198384155d679d13', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 13:56:32', '2021-05-14 13:56:32', '2022-05-14 06:56:32'),
('9e0c21e354655828419684d3277c4fbc4773c526ec963a1677e3e50873dc9f89d60a8c479e9b3bb0', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 14:04:17', '2021-05-14 14:04:17', '2022-05-14 07:04:17'),
('aa5650642551a9bed0d1f1a54d65966d3d58993f30ef1dcd2f278c043c527afa05e972b58195c084', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-13 16:51:56', '2021-05-13 16:51:56', '2022-05-13 09:51:56'),
('b133468bc7a54662e2ebdc989b6eaf278fab1d39b6bfb881a33a6612ccda01dc482d40cfef9cbb4f', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 14:00:33', '2021-05-14 14:00:33', '2022-05-14 07:00:33'),
('b845a76e03b42c54a97df8443171600e4c8b088fea54a979b7aeb3b4b055b44e7ae17ec181a26d8d', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 13:18:29', '2021-05-14 13:18:29', '2022-05-14 06:18:29'),
('bb6eece1ba19ebeafe6d88f0226d4701b9091d892056917eb8e38af31affd7767a927d972e35e1ab', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 13:18:30', '2021-05-14 13:18:30', '2022-05-14 06:18:30'),
('c46b3453c85d6f128ce7ab384550dd31cb5e94c70813cbb70618c415bfcc7a527db3ab6bab24b99d', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 13:02:04', '2021-05-14 13:02:04', '2022-05-14 06:02:04'),
('c4a8d0e6593131a0a368ac8227c246509f8909ab935e0727c5974699d2bdfa099d9b4d30997649f0', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-13 10:25:14', '2021-05-13 10:25:14', '2022-05-13 03:25:14'),
('c4dcedcb0ae18ecd1d157868141d0cb1ca3690002222229ce9bde499bade59e0e556a7fdd641ad83', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-13 16:50:25', '2021-05-13 16:50:25', '2022-05-13 09:50:25'),
('c72c4ff7596775a0ce6fbf2f98c8a03d2bbaed11a6b51e7d43f3c4b0485f7dfdf7890db2393dfa54', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 14:00:32', '2021-05-14 14:00:32', '2022-05-14 07:00:32'),
('c9de55ea7061cd978ff0d751a7312319065b45eaa757c8da50939037b8abfb06c93ee4131884f19c', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-13 10:18:42', '2021-05-13 10:18:42', '2022-05-13 03:18:42'),
('cb9717c9f0d5f6094ccbf4a643427e0693bb342793255b8e2e233ed9ea59ec1e204a4f9f8050bfa8', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 13:57:27', '2021-05-14 13:57:27', '2022-05-14 06:57:27'),
('cc7e9f1469ee169cd60ce1f3ae8453f0e2debfaeedd9b99ff6d1fd2eb87b047aa0d99043572fcd8d', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 14:00:34', '2021-05-14 14:00:34', '2022-05-14 07:00:34'),
('cd273f2702f160ddba213745bd9aa459112d27d32fc3ad0ca97b94ac48ef01d898e07e21e3ebdd73', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 14:03:30', '2021-05-14 14:03:30', '2022-05-14 07:03:30'),
('ce15964200802e9954dfe13d3eaf41d14701a69cc417074e056602c501e7ce12ff1d002f4e62aa52', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 13:59:52', '2021-05-14 13:59:52', '2022-05-14 06:59:52'),
('ce8f86cb999c48ac1e0f516334cb3e10624c7e4bafdc231966a29775c26428fc0ab7779f92a8bfa5', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 13:56:34', '2021-05-14 13:56:34', '2022-05-14 06:56:34'),
('e018dd48e84e6973e0e61e25e7ee329fbb10c79c23ef7c7a957a81dab56c78b9a8bca512659fd665', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 14:07:47', '2021-05-14 14:07:47', '2022-05-14 07:07:47'),
('e3025f7247542890b156dbf23ab9b63c124b9419fcfbf6e56b12791f7829fa708f37af6b5126045e', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-13 10:35:36', '2021-05-13 10:35:36', '2022-05-13 03:35:36'),
('e8be747e4e9e9ec4ae86f7537e8130f53dae0bb8330fffcb0f7b603d44bdfcdd888e4947f1572279', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 14:05:49', '2021-05-14 14:05:49', '2022-05-14 07:05:49'),
('ec3bc58016f011fecadef25e68e4500b694cdeb87f1064a746a149fe49671ca0f5cafe45d7094f48', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 13:59:55', '2021-05-14 13:59:55', '2022-05-14 06:59:55'),
('f0050fec77c6b58ca5dc51e24b89fe5486feb032e7476abff0d93dd462981fb7f908328af4ec5f40', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 13:57:31', '2021-05-14 13:57:31', '2022-05-14 06:57:31'),
('f130f96c831976c795ace3caac025f042fb189e8f2b4d26d41af40be704c301d4f396b0051d6b188', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-14 13:18:31', '2021-05-14 13:18:31', '2022-05-14 06:18:31'),
('f185e72cc51c35423a19db4b1d87b75e9bf97267edf03d7bbc638b5bc2e7fbcc43e24a6db96fc63c', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-13 10:32:14', '2021-05-13 10:32:14', '2022-05-13 03:32:14'),
('f1c45fa657c392b430f7b54ea2bcffdbda97253f12c86538a898d33843e5153644a5c1760417543b', 1, '936af4d8-990c-4137-a09d-d3ad721771af', 'authToken', '[]', 0, '2021-05-13 17:06:12', '2021-05-13 17:06:12', '2022-05-13 10:06:12'),
('fb32e18bcfe5436562d0ad99ea922906a7235685a98360d66d7f22d139c65490e50bc90a77260980', 3, '936af4d8-990c-4137-a09d-d3ad721771af', 'MyApp', '[]', 0, '2021-05-13 10:17:42', '2021-05-13 10:17:42', '2022-05-13 03:17:42');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
('936af4d8-990c-4137-a09d-d3ad721771af', NULL, 'Laravel Personal Access Client', 'IG9XQimjKyT1ec8cmVFu1ngz0BX9RTWpFYAhlN0b', NULL, 'http://localhost', 1, 0, 0, '2021-05-13 10:17:17', '2021-05-13 10:17:17'),
('936af4da-5125-4586-b2e6-7884b2d841d5', NULL, 'Laravel Password Grant Client', 'WHs8C9v8Y13WeTEa59hI4u01tNqsvOiKOYF3Ctlk', 'users', 'http://localhost', 0, 1, 0, '2021-05-13 10:17:17', '2021-05-13 10:17:17');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, '936af4d8-990c-4137-a09d-d3ad721771af', '2021-05-13 10:17:17', '2021-05-13 10:17:17');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sub_tasks`
--

CREATE TABLE `sub_tasks` (
  `id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `created_on` datetime NOT NULL,
  `task_name` text NOT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `total_time` time DEFAULT NULL,
  `status` enum('pending','inprogress','completed') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sub_tasks`
--

INSERT INTO `sub_tasks` (`id`, `task_id`, `user_id`, `name`, `created_on`, `task_name`, `start_time`, `end_time`, `total_time`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'Sub Task', '2021-05-14 00:00:00', 'Sub Task Sub Task Sub Task', NULL, NULL, NULL, 'pending', '2021-05-14 13:54:49', '2021-05-14 13:54:49', NULL),
(2, 3, 1, 'Sub Task', '2021-05-14 00:00:00', 'Sub Task Sub Task Sub Task', NULL, NULL, NULL, 'pending', '2021-05-14 13:55:06', '2021-05-14 13:55:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'task_created_by',
  `created_on` datetime NOT NULL DEFAULT current_timestamp(),
  `task_name` text NOT NULL,
  `status` enum('pending','inprogress','completed') NOT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `total_time` time DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `user_id`, `name`, `created_on`, `task_name`, `status`, `start_time`, `end_time`, `total_time`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'New Task', '2021-05-14 00:00:00', 'New Task New Task New Task', 'pending', NULL, NULL, NULL, '2021-05-14 13:53:26', '2021-05-14 13:53:26', NULL),
(2, 1, 'Sub Task', '2021-05-14 00:00:00', 'Sub Task Sub Task Sub Task', 'pending', NULL, NULL, NULL, '2021-05-14 13:54:57', '2021-05-14 13:54:57', NULL),
(3, 1, 'New Task', '2021-05-14 00:00:00', 'New Task New Task New Task', 'pending', NULL, NULL, NULL, '2021-05-14 13:55:06', '2021-05-14 13:55:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'test', 'test@gmail.com', '$2y$10$8Gfv0LeA5zCUPlPmnTOf1e5zOJLLbTJ.IxwoQuBW5obnHcZNauZFG', NULL, '2021-05-13 10:18:31', '2021-05-13 10:18:31'),
(15, 'test', 'tset@gmail.com', '$2y$10$0nVzZEN.EXasGO/MnMmZ7OSmooz4OO45zRtybbKiOtFI2VIv1cC86', NULL, '2021-05-13 16:43:00', '2021-05-13 16:43:00'),
(18, 'test', 'test@gmil.com', '$2y$10$gfOdqZRMh6q.eE6BOKBZg.H898lYAYX8v2HI4I484Dsz7Ob.RILoe', NULL, '2021-05-13 16:44:48', '2021-05-13 16:44:48');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `sub_tasks`
--
ALTER TABLE `sub_tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sub_tasks`
--
ALTER TABLE `sub_tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
