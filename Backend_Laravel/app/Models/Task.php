<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\SubTask;

class Task extends Model
{
    use SoftDeletes;
    protected $table = 'tasks';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'created_on',
        'task_name',
        'start_time',
        'end_time',
        'total_time',
        'status'
    ];

    public function subtasks(){
        return $this->hasMany(SubTask::class);
    }

}
