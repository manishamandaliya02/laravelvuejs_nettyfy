<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Task;

class SubTask extends Model
{
    use SoftDeletes;
    protected $table = 'sub_tasks';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'task_id',
        'user_id',
        'name',
        'created_on',
        'task_name',
        'start_time',
        'end_time',
        'total_time',
        'status'
    ];

    public function tasks(){
        return $this->blongsTo(Task::class,'task_id','id');
    }

}
