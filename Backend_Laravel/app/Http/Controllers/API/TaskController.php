<?php
   
namespace App\Http\Controllers\API;
   
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\City;
use App\Models\Task;
use App\Models\SubTask;
use Auth;
use Validator;
use Carbon\Carbon;
use DB;

class TaskController extends BaseController
{

    
    /**
     * Get Task List api
     *
     * @return \Illuminate\Http\Response
     */
    public function getTaskList($id)
    {
        try {
            $task = Task::where('user_id',$id)->get();                      
            
            return $this->sendResponse($task, 'Successfully.');
       } catch(\Exception  $e){ 

           return $this->sendError('Please try again', ['error'=>'Please try again']);
        }
        
    }

     /**
     * Get Task api
     *
     * @return \Illuminate\Http\Response
     */
    public function getTaskDetails($id)
    {
        try {
            $task = Task::with('subtasks')->where('id',$id)->first();                      
            
            return $this->sendResponse($task, 'successfully.');
       } catch(\Exception  $e){ 

           return $this->sendError('Please try again', ['error'=>'Please try again']);
        }
        
    }

     /**
     * Get Sub Task api
     *
     * @return \Illuminate\Http\Response
     */
    public function getSubTaskDetails($id)
    {
        try {
            $task = SubTask::find($id);                      
            
            return $this->sendResponse($task, 'successfully.');
       } catch(\Exception  $e){ 

           return $this->sendError('Please try again', ['error'=>'Please try again']);
        }
        
    }

     /**
     * Manage Time Task api
     *
     * @return \Illuminate\Http\Response
     */
    public function manageTime(Request $request)
    {
       try {
            $taskData = $request->all();
            $time = array();
            if($taskData['type'] == 'end'){
                $time = array('end_time' => date('H:i:s')); 
            }else  if($taskData['type'] == 'start'){
                $time =  array('start_time' => date('H:i:s'),'end_time' => null);  
            }
                            
           $data = DB::table('sub_tasks')
            ->where('id',$taskData['taskId'])
            ->update($time);

            if($data){
            $subtask = SubTask::find($taskData['taskId']); 
            
            $time = $subtask['start_time'];
            $time2 = $subtask['end_time'];

            $result = date("H:i:s",strtotime($time)-strtotime($time2));
            if($time2){
            $time3 = $subtask['total_time'];
            $time4 = $result;

            $secs2 = strtotime($time4);
            $result2 = date("H:i:s",strtotime($time3)+$secs2);

            $taskData = DB::table('sub_tasks')
            ->where('id',$taskData['taskId'])
            ->update(['total_time'=>$result2]);
            }

            return $this->sendResponse($result, 'successfully.');
            }
       } catch(\Exception  $e){ 

           return $this->sendError('Please try again', ['error'=>'Please try again']);
        }
        
    }
    
    /**
     * Task api
     *
     * @return \Illuminate\Http\Response
     */
    public function createTask(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'created_on' => 'required',
            'task_name' => 'required',
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
        try {
            $input = $request->all();
            if($input['id']){
                $task = Task::where('id',$input['id'])->update($input); 
            }else{
                $task = Task::create($input); 
            }
               
            return $this->sendResponse($task, 'Task successfully created.');
        } catch(\Exception  $e){ 

            return $this->sendError('Please try again', ['error'=>'Please try again']);
        }
        
    }

    /**
     * Sub Task api
     *
     * @return \Illuminate\Http\Response
     */
    public function createSubTask(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'created_on' => 'required',
            'task_name' => 'required',
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
        try {
            $input = $request->all();
            if($input['id']){
                $task = SubTask::where('id',$input['id'])->update($input); 
            }else{
                $task = SubTask::create($input); 
            }
                      
            
            return $this->sendResponse($task, 'Task successfully created.');
        } catch(\Exception  $e){ 

            return $this->sendError('Please try again', ['error'=>'Please try again']);
        }
        
    }

     /**
     * Clone Task api
     *
     * @return \Illuminate\Http\Response
     */
    public function cloneTask($task_id)
    {
        try {
            $task = Task::find($task_id);
            $newTask = $task->replicate();
            $newTask->created_at = Carbon::now();
            $newTask->save();    

            $subtasks = SubTask::where('task_id',$task_id)->get();  
            foreach ($subtasks as $key => $value) {
                $newTasks = $value->replicate();
                $newTasks->task_id = $newTask->id;
                $newTasks->created_at = Carbon::now();
                $newTasks->save();                 
            }      
           
            return $this->sendResponse($task_id, 'Task successfully clone.');
         } catch(\Exception  $e){ 

             return $this->sendError('Please try again', ['error'=>'Please try again']);
         }
        
    }

    /**
     * Clone Sub Task api
     *
     * @return \Illuminate\Http\Response
     */
    public function cloneSubTask($task_id)
    {
        try {
            $task = SubTask::find($task_id);
            
            $newTask = $task->replicate(['task_id']);
            $newTask->setTable('tasks');
            $newTask->created_at = Carbon::now();
            $newTask->save();            
            
            return $this->sendResponse($task, 'Task successfully clone.');
        } catch(\Exception  $e){ 

            return $this->sendError('Please try again', ['error'=>'Please try again']);
        }
        
    }
    

     /**
     * Delete Task api
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteTask($task_id)
    {
        try {
            $task = Task::find($task_id)->delete();                      
            
            return $this->sendResponse($task, 'Task successfully deleted.');
         } catch(\Exception  $e){ 

             return $this->sendError('Please try again', ['error'=>'Please try again']);
         }
        
    }

     /**
     * Delete Sub Task api
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteSubTask($task_id)
    {
        try {
            $task = SubTask::find($task_id)->delete();                      
            
            return $this->sendResponse($task, 'Sub Task successfully deleted.');
         } catch(\Exception  $e){ 

             return $this->sendError('Please try again', ['error'=>'Please try again']);
         }
        
    }

     /**
     * Change Task Status api
     *
     * @return \Illuminate\Http\Response
     */
    public function changeStatus(Request $request)
    {
        try {
            $taskData = $request->all();
            $task = DB::table('tasks')
            ->where('id',$taskData['taskId'])
            ->update(['status'=>$taskData['status']]);                      
            
            return $this->sendResponse($task, 'Task status successfully changed.');
         } catch(\Exception  $e){ 

             return $this->sendError('Please try again', ['error'=>'Please try again']);
         }
        
    }

     /**
     * Change Sub Task Status api
     *
     * @return \Illuminate\Http\Response
     */
    public function changeSubTaskStatus(Request $request)
    {
        try {
            $taskData = $request->all();
            $task = DB::table('sub_tasks')
            ->where('id',$taskData['taskId'])
            ->update(['status'=>$taskData['status']]);                      
            
            return $this->sendResponse($task, 'Task status successfully changed.');
         } catch(\Exception  $e){ 

             return $this->sendError('Please try again', ['error'=>'Please try again']);
         }
        
    }
    

}