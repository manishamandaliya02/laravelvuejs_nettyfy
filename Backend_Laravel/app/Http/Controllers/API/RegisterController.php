<?php
   
namespace App\Http\Controllers\API;
   
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\User;
use App\Models\UserHobby;
use Illuminate\Support\Facades\Auth;
use Validator;
use Log;
use Hash;
use Str;
use Illuminate\Database\QueryException;
   
class RegisterController extends BaseController
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'min:6',
            'password_confirmation' =>  'required_with:password|same:password|min:6',
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
        try {
            $input = $request->all();
            $input['password'] = bcrypt($input['password']); 
            $user = User::create($input);
            
            $user['token'] =  $user->createToken('MyApp')->accessToken;
            
            return $this->sendResponse($user, 'User register successfully.');
        } catch(\Exception  $e){ 

            $userEmail = User::select('*')->where('email',$input['email'])->count();
            if(($userEmail)>0){
                return $this->sendError('Email already exist.', ['email'=>['Email already exist.']]);
            }

            return $this->sendError('Please try again', ['error'=>'Please try again']);
        }
        
    }
   
}