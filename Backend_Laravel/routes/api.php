<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\RegisterController;
use App\Http\Controllers\API\LoginController;
use App\Http\Controllers\API\TaskController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/get-task/{id}', [TaskController::class, 'getTaskList']);
Route::delete('delete-task/{id}', [TaskController::class, 'deleteTask']);
Route::delete('delete-sub-task/{id}', [TaskController::class, 'deleteSubTask']);
Route::post('createTask', [TaskController::class, 'createTask']);
Route::post('create-sub-task', [TaskController::class, 'createSubTask']);
Route::post('clone-task/{id}', [TaskController::class, 'cloneTask']);
Route::post('clone-sub-task/{id}', [TaskController::class, 'cloneSubTask']);
Route::get('get-task-details/{id}', [TaskController::class, 'getTaskDetails']);
Route::get('get-sub-task-details/{id}', [TaskController::class, 'getSubTaskDetails']);
Route::post('manage-time', [TaskController::class, 'manageTime']);
Route::post('change-status', [TaskController::class, 'changeStatus']);
Route::post('change-sub-task-status', [TaskController::class, 'changeSubTaskStatus']);

Route::post('register', [RegisterController::class, 'register']);
Route::post('login', [LoginController::class, 'login']);


