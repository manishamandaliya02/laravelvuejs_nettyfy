const saveUser = ({ commit }, payload) => {
  setTimeout(() => {
    commit('setUser', payload)
  })
}

const updateUser = ({ commit }, payload) => {
  setTimeout(() => {
    commit('updateUser', payload)
  })
}

const LogOut = ({ commit }, payload) => {
  setTimeout(() => {
    commit('LogOut', payload)
  })
}

const loginUser = ({ commit }, payload) => {
  setTimeout(() => {
    commit('loginUser', payload)
  })
}

export default {
  saveUser,
  updateUser,
  LogOut,
  loginUser
}
