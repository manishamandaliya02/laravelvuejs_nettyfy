export default {
  setUser(state, payload) {
    const found = state.userList.find((u) => {
      if (u.email === payload.email) {
        return u
      }
    })

    if (found) {
      state.userList.map((u) => {
        if (u.email === payload.email) {
          u = payload
        }

        return u
      })
    } else {
      state.userList.push(payload)
    }
    state.user = payload
    localStorage.setItem('user', JSON.stringify(payload))
  },
  setRegistered(state, status) {
    state.isRegistered = status
  },
  LogOut(state) {
    console.log(state.user);
    state.isRegistered = false
    state.user = null

    console.log(state.user);
    localStorage.clear();
  },
  loginUser(state, user) {
    state.user = user;
    localStorage.setItem('user', JSON.stringify(user))
  }
}
