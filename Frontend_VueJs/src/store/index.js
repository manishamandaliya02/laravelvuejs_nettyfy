import Vue from 'vue'
import Vuex from 'vuex'

// Global vuex
import UserModule from './modules/users'

Vue.use(Vuex)

/**
 * Main Vuex Store
 */
const store = new Vuex.Store({
  modules: {
    user: UserModule
  },
})

export default store
