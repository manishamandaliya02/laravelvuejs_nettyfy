import Vue from 'vue';
import Router from 'vue-router';

import HelloWorld from '../components/HelloWorld'
import LoginPage from '../components/login/LoginPage'
import RegisterPage from '../components/register/RegisterPage'
import UserTaskPage from '../components/usertask/UserTaskPage'
import UserSubTaskPage from '../components/usertask/UserSubTaskPage'
import UserTaskListPage from '../components/usertask/UserTaskListPage'
import UserTaskDetailsPage from '../components/usertask/UserTaskDetailsPage'
import UserSubTaskDetailsPage from '../components/usertask/UserSubTaskDetailsPage'

Vue.use(Router);

export const router = new Router({
  mode: 'history',
  routes: [
    { path: '/', component: HelloWorld },
    { path: '/login', component: LoginPage },
    { path: '/register', component: RegisterPage },
    { path: '/task', component: UserTaskPage,name:'task' },
    { path: '/edittask/:id', component: UserTaskPage },
    { path: '/subtask/:id', component: UserSubTaskPage,name:'subtask' },
    { path: '/editsubtask/:id', component: UserSubTaskPage,name:'editsubtask' },
    { path: '/viewtask/:id', component: UserTaskDetailsPage },
    { path: '/viewsubtask/:id', component: UserSubTaskDetailsPage },
    { path: '/tasklist', component: UserTaskListPage },
    

    // otherwise redirect to home
    { path: '*', redirect: '/' }
  ]
});

router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const publicPages = ['/login', '/register'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem('user');

  if (authRequired && !loggedIn) {
    return next('/login');
  }

  next();
})
