import axios from 'axios'
const baseURL = 'http://localhost:8000/api/'

const handleError = (fn) => (...params) =>
  fn(...params).catch((error) => {
    console.log(error.response);
  })

export const api = {
//   gettask: handleError(async (id) => {
//     const res = await axios.get(baseURL + id)

  //     return res.data
  //   }),
  //   gettasks: handleError(async () => {
  //     const res = await axios.get(baseURL)

  //     return res.data
  //   }),
  //   deletetask: handleError(async (id) => {
  //     const res = await axios.delete(baseURL + id)

  //     return res.data
  //   }),
  login: handleError(async (payload) => {
    const res = await axios.post(baseURL + 'login', payload)
    return res
  }),
  register: handleError(async (payload) => {
      const res = await axios.post(baseURL + 'register', payload)
      return res
  }),
  getUserId:handleError(async (payload) => {
    const res = await axios.get(`${baseURL}email?email=${payload.email}`,payload)
    return res.data
  }),
  createTask: handleError(async (payload) => {
    const res = await axios.post(baseURL + 'createTask', payload)

    return res.data
  }),
  getTask: handleError(async (userId) => {
    const res = await axios.get(baseURL + 'get-task/'+userId)

    return res.data
  }),
  deleteTask: handleError(async (taskId) => {
    const res = await axios.delete(baseURL + 'delete-task/'+taskId)

    return res.data
  }),
  deleteSubTask: handleError(async (taskId) => {
    const res = await axios.delete(baseURL + 'delete-sub-task/'+taskId)

    return res.data
  }),
  cloneTask: handleError(async (taskId) => {
    const res = await axios.post(baseURL + 'clone-task/'+taskId)

    return res.data
  }),
  cloneSubTask: handleError(async (taskId) => {
    const res = await axios.post(baseURL + 'clone-sub-task/'+taskId)

    return res.data
  }),
  createSubTask: handleError(async (payload) => {
    const res = await axios.post(baseURL + 'create-sub-task',payload)

    return res.data
  }),
  viewTask: handleError(async (taskId) => {
    const res = await axios.get(baseURL + 'get-task-details/'+taskId)

    return res.data
  }),
  viewSubTask: handleError(async (taskId) => {
    const res = await axios.get(baseURL + 'get-sub-task-details/'+taskId)

    return res.data
  }),
  manageTime: handleError(async (type,taskId) => {
    const res = await axios.post(baseURL + 'manage-time/',{type:type,taskId:taskId})

    return res.data
  }),
  manageSubTaskTime: handleError(async (type,taskId) => {
    const res = await axios.post(baseURL + 'manage-sub-time/',{type:type,taskId:taskId})

    return res.data
  }),
  changeSubTaskStatus: handleError(async (status,taskId) => {
    const res = await axios.post(baseURL + 'change-sub-task-status/',{status:status,taskId:taskId})

    return res.data
  }),
  changeStatus: handleError(async (status,taskId) => {
    const res = await axios.post(baseURL + 'change-status/',{status:status,taskId:taskId})

    return res.data
  }),
  updatetask: handleError(async (payload) => {
    const res = await axios.put(baseURL + 'create-sub-task', payload)

    return res.data
  }),
  updateUser:(async (user) => {
    const config = {
      headers: {
        'content-type': 'multipart/form-data'
      }
    };
     let fd = new FormData();
    let personalData =JSON.stringify({
        height: user.peronalizedInformation?user.peronalizedInformation.height:'',
        weight: user.peronalizedInformation?user.peronalizedInformation.weight:'',
        date: user.peronalizedInformation?user.peronalizedInformation.date:'',
    })
    console.log('user.user_roleuser.user_role',user.user_role);
    if(user.user_role == 1){
    let locaaddress = JSON.stringify({
        organizationName: user.location?user.location.organizationName:'',
        address: {
          locality: user.location?user.location.address?user.location.address.locality:'':'',
          city: user.location?user.location.address?user.location.address.city:'':'',
          state: user.location?user.location.address?user.location.address.states:'':'',
          zip: user.location?user.location.address?user.location.address.zipcode:'':'',
          country: user.location?user.location.address?user.location.address.country:'':''
        }
    })
    
    fd.append('location',locaaddress)
  }else{
    let locaaddress = JSON.stringify({
      organizationName: user.location?user.location.organizationName:'',
      address: {
        city: user.location?user.location.city:'',
        country: user.location?user.location.country:''
      }
  })
  
  fd.append('location',locaaddress)
  }
     fd.append('user_role',user.user_role)
    fd.append('tagline',user.tagline)
    fd.append('standardRate',user.standardRate)
    fd.append('sports',user.sports)
    fd.append('skillBuilding',user.skillBuilding)
    fd.append('profilePicture',user.profilePicture)
    fd.append('profileDescription',user.profileDescription)
    fd.append('peronalizedInformation',personalData)
    fd.append('name',user.name)
    fd.append('level',user.level)
    fd.append('email',user.email)
    fd.append('bannerImage',user.bannerImage)
    fd.append('athleticWeakness',user.athleticWeakness)
    fd.append('athleticSkills',user.athleticSkills)
    fd.append('athleticInterests',user.athleticInterests)
    fd.append('athleticGoals',user.athleticGoals)
    // const res12 = await axios.patch(baseURL + 'user_update/'+user.user._id, fd,config)
    const res = await axios.patch(baseURL + 'user_update/'+user._id, fd,config)
    console.log(user)
    return res.data
  })
}